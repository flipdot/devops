#!/bin/bash

select filename in inventories/*
do
    if [[ "$filename" == "" ]]
    then
        echo "'$REPLY' is not a valid number"
        continue
    fi

    # now we can use the selected file
    echo "Deploying $filename..."
    ansible-playbook -i $filename --ask-vault-pass $@

    # it'll ask for another unless we leave the loop
    break
done
