# flipdot infrastructure as code

This repository contains the configuration of some flipdot servers. Currently, the following services are managed
here:

* The bind9 nameserver for https://flipdot.dev (TODO: The machine the nameserver currently runs on is privately owned.)
* The hedgedoc on https://pad.flipdot.org
* The flipbot, which posts stuff like the plenum reminder in our [discourse instance](https://forum.flipdot.org)
    * Source of the flipbot [at GitHub](https://github.com/flipdot/forumbot)
* The watchtower which keeps docker containers up to date
